<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Imanuel', 'univ' => 'UKDW', 'asal' => 'Tegal'],
            ['name' => 'Kiki', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
            ['name' => 'Edwin', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
            ['name' => 'Novianto', 'univ' => 'Atma', 'asal' => 'Sumsel'],
            ['name' => 'Fatur', 'univ' => 'UPN', 'asal' => 'Yogyakarta'],
        ];

        DB::beginTransaction();

        foreach($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
