<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class DataDiriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datadiri()
    {
        // $data=[
        //     ['nama' => 'Imanuel', 'univ' => 'UKDW', 'asal' => 'Tegal'],
        //     ['nama' => 'Kiki', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Edwin', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Novianto', 'univ' => 'Atma', 'asal' => 'Sumsel'],
        // ];
        $data = Member::all();
        return view('datadiri/table', [
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $detail = Member::find($id);
        return view('detailmember/detail', compact('detail','request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $detail = Member::find($id);
        return view('detailmember/detail', compact('detail','request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $updatemember = $member->find($request->id);
        $updatemember->id = $request->id;
        $updatemember->name = $request->name;
        $updatemember->univ = $request->univ;
        $updatemember->asal = $request->asal;
        if($updatemember->save()){
            return redirect('/table');
        }
        else{
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = Member::destroy($id);
        return redirect('/table');
    }
}
