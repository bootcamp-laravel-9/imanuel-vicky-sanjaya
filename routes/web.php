<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\CvController;
use App\Http\Controllers\DataDiriController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard.index');
// });
Route::get('/', 'UserController@dashboard');
Route::get('/table', 'DataDiriController@datadiri');
Route::get('/cv', 'CvController@civi');
Route::get('/member-detail/{id}', 'DataDiriController@show');
Route::get('/member-edit/{id}', 'DataDiriController@edit');
Route::post('/member-update', 'DataDiriController@update');
Route::get('/member-delete/{id}', 'DataDiriController@destroy');
Route::get('/login', 'AuthController@login');
Route::get('/regis', 'AuthController@regis');
