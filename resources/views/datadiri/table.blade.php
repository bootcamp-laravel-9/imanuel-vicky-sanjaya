@extends('layout/main')
@section('content')
@section('title', 'Peserta Bootcamp')
@section('menu-data', 'active')
<!-- /.row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Peserta Bootcamp</h3>

                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Asal Universitas</th>
                            <th>Asal Daerah</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $index => $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row['name'] }}</td>
                                <td
                                    class="{{ $row['univ'] == 'Atma' ? 'text-green' : ($row['univ'] == 'UKDW' ? 'text-blue' : ($row['univ'] == 'UPN' ? 'text-red' : '')) }}">
                                    {{ $row['univ'] }}</td>
                                <td>{{ $row['asal'] }}</td>
                                <td>
                                    <a href="{{url('/member-edit/'.$row->id)}}" class="btn btn-warning">Edit</a>
                                    <a href="{{url('/member-detail/'.$row->id)}}" class="btn btn-primary">Detail</a>
                                    <a href="{{url('/member-delete/'.$row->id)}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection
