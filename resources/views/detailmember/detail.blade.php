@extends('layout.main')
@section('title', 'Detail Member')
@section('content')
<div class="container-fluid">
    @if($request->segment(1) == 'member-edit')
        <form action="{{url('/member-update')}}" method="POST">
            @csrf
            <input name="id" type="hidden" class="form-control" id="exampleInputid" placeholder="Enter email" value="{{$detail['id']}}">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input name="name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="{{$detail['name']}}">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Asal Kampus</label>
                <input name="univ" type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" value="{{$detail['univ']}}">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword2">Asal Daerah</label>
                <input name="asal" type="text" class="form-control" id="exampleInputPassword2" placeholder="Password" value="{{$detail['asal']}}">
              </div>
              <div class="form-group">
                <input type="submit" title="Save">
              </div>
          </form>
    @elseif ($request->segment(1) == 'member-detail')
    <form>
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" readonly class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="{{$detail['name']}}">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Asal Kampus</label>
            <input type="text" readonly class="form-control" id="exampleInputPassword1" placeholder="Password" value="{{$detail['univ']}}">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword2">Asal Daerah</label>
            <input type="text" readonly class="form-control" id="exampleInputPassword2" placeholder="Password" value="{{$detail['asal']}}">
          </div>
      </form>
    @endif
</div>
@endsection
