@extends('layout/main')
@section('content')
@section('title', 'Circulum Vitae')
@section('menu-cv', 'active')
<div class="container-fluid">
    <!-- Informasi Pribadi -->
    <div id="informasi-pribadi" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Informasi Pribadi</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ asset('AdminLTE') }}/dist/img/user1-128x128.jpg" alt="User Avatar" />
                        </div>
                        <div class="col-md-6">
                            <strong>Name:</strong> John Doe<br>
                            <strong>Date of Birth:</strong> 10 Januari 1990<br>
                            <strong>Address:</strong> Jl. Contoh No. 123, Kota Contoh<br>
                            <strong>Email:</strong> johndoe@example.com<br>
                            <strong>Phone:</strong> 081234567890<br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.informasi pribadi -->

    <!-- Ringkasan Profil -->
    <div id="ringkasan-profil" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Ringkasan Profil</h3>
                </div>
                <div class="card-body">
                    <p>I am John Doe, a dedicated and detail-oriented professional with a passion for
                        continuous learning and development. Born on January 10, 1990, I currently reside at
                        Jl. Contoh No. 123, in the city of Contoh. I hold Indonesian citizenship and
                        maintain an active presence on LinkedIn (linkedin.com/in/johndoe) and GitHub
                        (github.com/johndoe). My educational background includes a degree in Computer
                        Science from XYZ University, where I developed a strong foundation in programming
                        and software development. With experience in various programming languages and
                        frameworks, I have successfully contributed to several projects, demonstrating my
                        ability to work collaboratively and deliver high-quality results. I am eager to
                        leverage my skills and expertise to contribute meaningfully to innovative projects
                        and contribute positively to the team and organization I join.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.ringkasan profil -->

    <!-- Riwayat Pendidikan -->
    <div id="riwayat-pendidikan" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Riwayat Pendidikan</h3>
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <strong>Bachelor of Science in Computer Science</strong><br>
                            <strong>University:</strong> XYZ University, Contoh<br>
                            <strong>Graduated:</strong> May 2014<br>
                            <strong>Relevant coursework:</strong> Data Structures, Algorithms, Software
                            Engineering, Database Management Systems, Web Development.<br>
                            <strong>Achievements:</strong> Dean's List for academic excellence.
                        </li>
                        <li>
                            <strong>High School Diploma</strong><br>
                            <strong>School:</strong> Contoh High School<br>
                            <strong>Graduated:</strong> June 2010<br>
                            <strong>Achievements:</strong> Member of the Science Club, participated in
                            regional programming competitions.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.riwayat pendidikan -->

    <!-- Pengalaman Kerja -->
    <div id="pengalaman-kerja" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Pengalaman Kerja</h3>
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <strong>Software Developer</strong><br>
                            <strong>Company:</strong> ABC Tech Solutions, Contoh<br>
                            <strong>Duration:</strong> June 2014 - Present<br>
                            <strong>Responsibilities:</strong>
                            <ul>
                                <li>Developed and maintained web applications using HTML, CSS, JavaScript,
                                    and PHP.</li>
                                <li>Collaborated with cross-functional teams to deliver projects on time and
                                    within budget.</li>
                                <li>Participated in code reviews and provided technical support to team
                                    members.</li>
                                <li>Implemented best practices for code optimization and performance
                                    improvement.</li>
                            </ul>
                        </li>
                        <li>
                            <strong>Internship - Software Engineer</strong><br>
                            <strong>Company:</strong> XYZ Software Development, Contoh<br>
                            <strong>Duration:</strong> May 2013 - August 2013<br>
                            <strong>Responsibilities:</strong>
                            <ul>
                                <li>Assisted in developing and testing software modules for a new product
                                    launch.</li>
                                <li>Participated in team meetings and contributed ideas for product
                                    enhancements.</li>
                                <li>Learned and applied agile development methodologies under supervision.
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.pengalaman kerja -->

    <!-- Keahlian dan Kualifikasi -->
    <div id="keahlian-kualifikasi" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Keahlian dan Kualifikasi</h3>
                </div>
                <div class="card-body">
                    <h4>Technical Skills:</h4>
                    <ul>
                        <li>Programming Languages: Java, Python, JavaScript, PHP</li>
                        <li>Web Technologies: HTML5, CSS3, Bootstrap, React.js</li>
                        <li>Database Management: MySQL, MongoDB</li>
                        <li>Version Control: Git, SVN</li>
                        <li>Operating Systems: Windows, Linux</li>
                    </ul>
                    <h4>Soft Skills:</h4>
                    <ul>
                        <li>Effective communication and interpersonal skills</li>
                        <li>Problem-solving and analytical thinking</li>
                        <li>Teamwork and collaboration</li>
                        <li>Time management and organizational skills</li>
                        <li>Adaptability and willingness to learn new technologies</li>
                    </ul>
                    <h4>Qualifications:</h4>
                    <ul>
                        <li>Bachelor of Science in Computer Science, XYZ University</li>
                        <li>Certified Scrum Master (CSM)</li>
                        <li>Google Cloud Platform (GCP) Certification</li>
                        <li>TOEFL iBT Score: 110/120</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.keahlian dan kualifikasi -->

    <!-- Sertifikasi dan Pelatihan -->
    <div id="sertifikasi-pelatihan" class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Sertifikasi dan Pelatihan</h3>
                </div>
                <div class="card-body">
                    <h4>Certifications:</h4>
                    <ul>
                        <li>Certified Scrum Master (CSM)</li>
                        <li>Google Cloud Platform (GCP) Certification</li>
                        <li>Microsoft Certified: Azure Developer Associate</li>
                        <li>Oracle Certified Professional, Java SE Programmer</li>
                    </ul>
                    <h4>Training Programs:</h4>
                    <ul>
                        <li>Agile Software Development Training</li>
                        <li>Data Science and Machine Learning Bootcamp</li>
                        <li>Web Development Workshop</li>
                        <li>Cybersecurity Essentials Training</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.sertifikasi dan pelatihan -->
</div>
<!-- /.container-fluid -->
@endsection
